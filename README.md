### 更改内容
1. 将所有请求的url统一采用相对路径的方式，如将http://114.55.103.226/maternity/api/user/login改为./api/login
2. add_matron.js
	1. 修改发送数据的格式，trianning和working_photo的信息均属于certificates内，之前为并列关系
	2. 将时间信息（健康证开始时间等）转换为Date格式传输（line 55）
	3. grade,lanuageh和level均使用默认值传输（level值应为int类型，之前给你的接口这个地方有点问题）（line 168-176）
	4. 发送的数据中增加了身份证号码和健康证时间信息的获取
	5. 增加数据添加成功后跳转至overview_page.html（line 224）
3. matron_info.js
	1. 去掉了一些alert命令
	2. 新增了解析时间的例子（line 183）
	3. 以及修改请求的url为根据ID生成（line 101）

20160912修改内容
matron_info.js:增加是否护理双胞胎的经验显示（line 364）
add_matron.js:修改上传的护理双胞胎经验格式为num_of_twins（line 223）
