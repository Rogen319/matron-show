/**
 * Created by 文海 on 2016/9/11.
 */

/*
 * 显示用户名字
 * */
var addMaterialImgTemplate;
var loadBody = function () {
    var username = sessionStorage.getItem("maternity_name");
    if (username == null) {
        alert("请先登录");
        location.href = "login.html";
    }
    else {
        document.getElementById("maternity_name").innerHTML = sessionStorage.getItem("maternity_name");
    }
    // 绑定上传配置
    bind_upload();
    addMaterialImgTemplate = $('div.material-img:last').prop('outerHTML');
    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);
    $('.date-picker').datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true
    });
};

/*
 * 登出
 * */
var logout = function () {
    sessionStorage.clear();
    location.href = "login.html";
}

/*
 * 加载id对应的月嫂信息
 * */
var app = angular.module('myApp', []);
app.controller('infoCtrl', function ($scope, $compile, $http) {

    //记录证书是否存在，默认不存在
    var isExist = new Array(10);
    for (var i = 0; i < 10; i++) {
        isExist[i] = false;
    }

    $scope.setNoneDisplay = function () {
        document.getElementById('yyy').style.display = "none";
        document.getElementById('jzfwy').style.display = "none";
        document.getElementById('yypcy').style.display = "none";
        document.getElementById('byy').style.display = "none";
        document.getElementById('myhls').style.display = "none";
        document.getElementById('yys').style.display = "none";
        document.getElementById('hsbyz').style.display = "none";
        document.getElementById('crs').style.display = "none";
        document.getElementById('xjys').style.display = "none";
        document.getElementById('myjkzds').style.display = "none";
    }
    $scope.setAllDisplay = function () {
        if (isExist[0] == true) {
            document.getElementById('yyy').style.display = "inline-block";
        }
        if (isExist[1] == true) {
            document.getElementById('jzfwy').style.display = "inline-block";
        }
        if (isExist[2] == true) {
            document.getElementById('yypcy').style.display = "inline-block";
        }
        if (isExist[3] == true) {
            document.getElementById('byy').style.display = "inline-block";
        }
        if (isExist[4] == true) {
            document.getElementById('myhls').style.display = "inline-block";
        }
        if (isExist[5] == true) {
            document.getElementById('yys').style.display = "inline-block";
        }
        if (isExist[6] == true) {
            document.getElementById('hsbyz').style.display = "inline-block";
        }
        if (isExist[7] == true) {
            document.getElementById('crs').style.display = "inline-block";
        }
        if (isExist[8] == true) {
            document.getElementById('xjys').style.display = "inline-block";
        }
        if (isExist[9] == true) {
            document.getElementById('myjkzds').style.display = "inline-block";
        }
    }

    $scope.setNoneDisplay();
    $scope.setSelectedCertification = function (x) {
        $scope.setNoneDisplay();
        if (x == "myhls") {
            if (isExist[4]) {
                document.getElementById('myhls').style.display = "block";
            }
            else {
                alert("该月嫂未上传此证书！");
            }
        }
        else if (x == "yyy") {
            if (isExist[0] == true) {
                document.getElementById('yyy').style.display = "block";
            }
            else {
                alert("该月嫂未上传此证书！");
            }
        }
        else if (x == "jzfwy") {
            if (isExist[1] == true) {
                document.getElementById('jzfwy').style.display = "block";
            }
            else {
                alert("该月嫂未上传此证书！");
            }
        }
        else if (x == "yypcy") {
            if (isExist[2] == true) {
                document.getElementById('yypcy').style.display = "block";
            }
            else {
                alert("该月嫂未上传此证书！");
            }
        }
        else if (x == "byy") {
            if (isExist[3] == true) {
                document.getElementById('byy').style.display = "block";
            }
            else {
                alert("该月嫂未上传此证书！");
            }
        }
        else if (x == "hsbyz") {
            if (isExist[6] == true) {
                document.getElementById('hsbyz').style.display = "block";
            }
            else {
                alert("该月嫂未上传此证书！");
            }
        }
        else if (x == "crs") {
            if (isExist[7] == true) {
                document.getElementById('crs').style.display = "block";
            }
            else {
                alert("该月嫂未上传此证书！");
            }
        }
        else if (x == "xjys") {
            if (isExist[8] == true) {
                document.getElementById('xjys').style.display = "block";
            }
            else {
                alert("该月嫂未上传此证书！");
            }
        }
        else if (x == "yys") {
            if (isExist[5] == true) {
                document.getElementById('yys').style.display = "block";
            }
            else {
                alert("该月嫂未上传此证书！");
            }
        }
        else if (x == "myjkzds") {
            if (isExist[9] == true) {
                document.getElementById('myjkzds').style.display = "block";
            }
            else {
                alert("该月嫂未上传此证书！");
            }
        }
        else {
            $scope.setAllDisplay();
        }
    }

    var hy = "";
    var sy = "";
    var xl = "";
    var hljy = "";
    var yy = new Array();
    var ywcpx = new Array();
    var ggyy = new Array();
    var hljt = new Array();

    var id = sessionStorage.getItem("matron_id");
    //获取uid以及access_token
    var access_token = $.cookie("access_token");
    var uid = sessionStorage.getItem("uid");
    var url = ip + "yuesao/" + id + "/detail";

    //修改月嫂信息
    $scope.saveMatron = function () {
        //获取uid以及access_token
        var access_token = $.cookie("access_token");
        var uid = sessionStorage.getItem("uid");

        var name = $scope.name;
        var age = $scope.age;
        var province = document.getElementById("province");
        var index = province.selectedIndex;
        var birth_place = province.options[index].value;
        var phone = $scope.phone_num;
        var emergency = $scope.emergency;
        var relation = $scope.relation;
        var contact = $scope.contact;
        var id_num = $scope.id_num;
        var health_start = $scope.health_start;
        var health_end = $scope.health_end;
        var hlsj = new Date($scope.hlsj);
        var jtsl = $scope.jtsl;
        var health_start = new Date($scope.health_start);
        var health_end = new Date($scope.health_end);

        //婚姻状况
        var obj = document.getElementsByName("hyzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                hy = obj[i].nextSibling.innerText;
            }
        }

        //生育状况
        obj = document.getElementsByName("syzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                sy = obj[i].nextSibling.innerText;
            }
        }

        //学历状况
        obj = document.getElementsByName("xlzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                xl = obj[i].nextSibling.innerText;
            }
        }

        //语言能力
        obj = document.getElementsByName("yynl");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                yy.push(obj[i].nextSibling.innerText);
            }
        }

        //已完成培训
        obj = document.getElementsByName("ywcpx");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                var temp;
                if (obj[i].checked) {
                    temp = obj[i].nextSibling;
                    if (temp.nodeName == "SPAN") {
                        ywcpx.push(obj[i].nextSibling.innerText);
                    }
                    else {
                        ywcpx.push(obj[i].nextSibling.value);
                    }
                }
            }
        }

        //跟岗医院
        obj = document.getElementsByName("ggyy");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                var temp;
                if (obj[i].checked) {
                    temp = obj[i].nextSibling;
                    if (temp.nodeName == "SPAN") {
                        ggyy.push(obj[i].nextSibling.innerText);
                    }
                    else {
                        ggyy.push(obj[i].nextSibling.value);
                    }
                }
            }
        }

        //护理双胞胎经验
        obj = document.getElementsByName("hljy");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                hljy = obj[i].nextSibling.innerText;
            }
        }

        //护理家庭类型
        obj = document.getElementsByName("hljt");
        for (var i = 0; i < obj.length; i++) {
            var temp;
            if (obj[i].checked) {
                temp = obj[i].nextSibling;
                if (temp.nodeName == "SPAN") {
                    hljt.push(obj[i].nextSibling.innerText);
                }
                else {
                    hljt.push(obj[i].nextSibling.value);
                }
            }
        }

        var head_img_url = $('img.head-img').attr('data-url');
        var intro_video_url = $('video.intro-video').attr('data-url');
        var id_front_img_url = $('img.front_id_img').attr('data-url');
        var id_back_img_url = $('img.back_id_img').attr('data-url');
        var health_img_url = $('img.health_img').attr('data-url');
        var skills = [];
        $('.certification-img:visible').each(function () {
            var skill = {};
            skill.name = $(this).children('.certification-name').children('span').text();
            var rank_select = $(this).find('rank-select');
            if (rank_select != null) {
                skill.level = parseInt(rank_select.val());
            }
            skill.num = $(this).find('.certification-num').val();
            skill.img_url = $(this).find('img.skill-img').attr('data-url');
            skills.push(skill);
        });
        var working_photo = [];
        $('img.material-photo').each(function () {
            working_photo.push($(this).attr('data-url'));
        });
        var working_photo_tag = [];
        if(typeof($scope.tag1) != "undefined"){
            working_photo_tag.push($scope.tag1);
        }
        if(typeof($scope.tag2) != "undefined"){
            working_photo_tag.push($scope.tag2);
        }
        if(typeof($scope.tag3) != "undefined"){
            working_photo_tag.push($scope.tag3);
        }
        if(typeof($scope.tag4) != "undefined"){
            working_photo_tag.push($scope.tag4);
        }

        // 判断输入合法性
        if (!/\d{1,2}/.test(age) || age > 80 || age < 18) {
            alert("请填写正确的年龄");
            return;
        }

        if (!/\d{11}/.test(phone)) {
            alert("请输入11位手机号码");
            return;
        }

        if (!/\d{11}/.test(contact)) {
            alert("请输入11位手机号码");
            return;
        }

        if (!/\d{17}(\d|x)/.test($scope.id_num)) {
            alert("请输入正确的身份证号");
            return;
        }

        if (!checkDate(health_start,health_end)) {
            alert("健康证有效期无效");
            return;
        }

        {
            var curTime = new Date().getTime();
            var hlsjTime = hlsj.getTime();
            if (curTime<hlsjTime) {
                alert("最早护理时间无效");
                return;
            }
        }

        if (!/\d{1,2}/.test(jtsl) || jtsl < 0 || jtsl >99) {
            alert('护理家庭数量不合法，请输入0~99之间的数字');
            return;
        }

        if (name.length>20) {
            alert("姓名过长，最多20字符");
            return;
        }
        if (emergency.length>20) {
            alert("紧急联系人姓名过长，最多20字符");
            return;
        }
        if (relation.length>20) {
            alert("与紧急联系人关系过长，最多20字符");
            return;
        }

        //通过http添加月嫂
        $http({

            method: "put",
            url: url,
            data: {
                uid: uid,
                access_token: access_token,
                name: name, // 月嫂姓名
                age: age, // 月嫂年龄
                birth_place: birth_place, // 月嫂籍贯
                phone_num: phone, // 月嫂联系电话
                head_img_url: head_img_url, // 月嫂头像地址
                intro_video_url: intro_video_url, // 月嫂介绍视频地址
                educationBackground: xl,
                emergencyContactPersonName: emergency,
                emergencyContactPersonRelation: relation,
                emergencyContactPersonNumber: contact,
                procreationState: sy,
                language: yy,
                level: 5,//页面中没有相关选项
                grade: { // 月嫂评分//页面中没有相关选项 其中各项的值代表该评分的数量
                    one: 1,
                    two: 2,
                    three: 3,
                    four: 4,
                    five: 5
                },
                certificates: { // 证书资料
                    id_card: { // 身份证
                        num: $scope.id_num, // 证号
                        front_url: id_front_img_url, // 正面图片地址
                        back_url: id_back_img_url // 背面图片地址
                    },
                    health: { // 健康证
                        validity_period: { // 有效期
                            start: $scope.health_start, // 起始时间
                            end: $scope.health_end // 失效时间
                        },
                        img_url: head_img_url // 证件图片地址
                    },
                    skills: skills, // 技能证书
                    training: {
                        subjects: ywcpx, // 已完成培训项目，催乳师培训等
                        hospital: ggyy // 跟岗医院
                    },
                    nursing: {
                        first_time: $scope.hlsj, // 最早护理时间
                        num_of_family: jtsl, // 护理家庭数量
                        twins: hljy, // 有无护理双胞胎经验
                        family_type: // 家庭类型 双外籍、单外籍等
                        hljt,
                        location: []
                    },
                    working_photo: working_photo,// 工作照链接地址
                    working_photo_tag:working_photo_tag
                }
            }
        }).success(function (data, status, headers, config) {
            if (data.success) {
                alert("success");
                location.href = "overview_page.html"
            }
            else {
                alert(data.msg);
            }
        });
    };
    $scope.addIDImg = function () {
        document.getElementById('id1-img-input').click();
    };
    $scope.addHealthImg = function () {
        document.getElementById('health-img-input').click();
    };
    $scope.add_ywcpx = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ywcpx'/><input type='text' placeholder='自定义' style='width:84px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var ywcpx_div = document.getElementById("ywcpx_div");
        angular.element(ywcpx_div).append(dynamicYwcpxElement);
    }

    $scope.add_ggyy = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ggyy'/><input type='text' placeholder='自定义' style='width:45px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var ggyy_div = document.getElementById("ggyy_div");
        angular.element(ggyy_div).append(dynamicYwcpxElement);
    }

    $scope.add_hljt = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='hljt'/><input type='text' placeholder='自定义' style='width:70px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var hljt_div = document.getElementById("hljt_div");
        angular.element(hljt_div).append(dynamicYwcpxElement);
    }

    //获取月嫂信息
    $http({

        method: "post",
        url: url,
        data: {
            uid: uid,
            access_token: access_token
        }
    }).success(function (data, status, headers, config) {
        if (data.success) {

            //get certificates json array, obj[0] is json object
            var obj = eval('(' + data.data.certificates + ')');

            $scope.name = data.data.name;
            $scope.age = data.data.age;

            //根据birth_place设定province的选中项
            var provinceSel = document.getElementById("province");
            for (var i = 0; i < provinceSel.options.length; i++) {
                if (provinceSel.options[i].innerHTML == data.data.birth_place) {
                    provinceSel.options[i].selected = true;
                    break;
                }
            }

            $scope.phone_num = data.data.phone_num;
            $scope.id_num = obj[0].id_card.num;
            $scope.emergency = data.data.emergencyContactPersonName;
            $scope.relation = data.data.emergencyContactPersonRelation;
            $scope.contact = data.data.emergencyContactPersonNumber;

            switch (data.data.educationBackground) {
                case "小学":
                    document.getElementById("xl_xx").checked = true;
                    break;
                case "初中":
                    document.getElementById("xl_cz").checked = true;
                    break;
                case "中专":
                    document.getElementById("xl_zz").checked = true;
                    break;
                case "高中":
                    document.getElementById("xl_gz").checked = true;
                    break;
                case "大专":
                    document.getElementById("xl_dz").checked = true;
                    break;
            }

            if (data.data.procreationState == "已育") {
                document.getElementById("sy_yy").checked = true;
            }
            else {
                document.getElementById("sy_wy").checked = true;
            }
            if (data.data.marriageState == "已婚") {
                document.getElementById("hy_yh").checked = true;
            }
            else {
                document.getElementById("hy_wh").checked = true;
            }

            for (var i = 0; i < data.data.language.length; i++) {
                if (data.data.language[i] == "家乡话")
                    document.getElementById("jxh").checked = true;
                if (data.data.language[i] == "普通话")
                    document.getElementById("pth").checked = true;
                if (data.data.language[i] == "上海话")
                    document.getElementById("shh").checked = true;
                if (data.data.language[i] == "英语")
                    document.getElementById("english").checked = true;
            }

            var health_start = new Date(obj[0].health.validity_period.start);
            $scope.health_start = health_start.getFullYear() + "-" + (health_start.getMonth() + 1) + "-" + health_start.getDate();
            var health_end = new Date(obj[0].health.validity_period.end);
            $scope.health_end = health_end.getFullYear() + "-" + (health_end.getMonth() + 1) + "-" + health_end.getDate();
            var first_time_date = new Date(obj[0].nursing.first_time);
            $scope.hlsj = first_time_date.getFullYear() + "-" + (first_time_date.getMonth() + 1) + "-" + first_time_date.getDate();
            $scope.jtsl = obj[0].nursing.num_of_family;

            var head_img = document.getElementById("head_img");
            head_img.src = data.data.head_img_url;
            var intro_video = document.getElementById("intro_video");
            intro_video.src = data.data.intro_video_url;

            var id_front_img = document.getElementById("id_front_img");
            id_front_img.src = obj[0].id_card.front_url;
            var id_back_img = document.getElementById("id_back_img");
            id_back_img.src = obj[0].id_card.back_url;

            var health_img = document.getElementById("health_img");
            health_img.src = obj[0].health.img_url;

            //设置育婴员资格证
            var yyy_div = document.getElementById("yyy");
            var yyy_img = document.getElementById("yyy_img");
            //var yyy_level = document.getElementById("yyy_level");
            //var yyy_serial = document.getElementById("yyy_serial");

            //设置家政服务员资格证
            var jzfwy_div = document.getElementById("jzfwy");
            var jzfwy_img = document.getElementById("jzfwy_img");
            //var jzfwy_level = document.getElementById("jzfwy_level");
            //var jzfwy_serial = document.getElementById("jzfwy_serial");

            //设置营养配餐员资格证
            var yypcy_div = document.getElementById("yypcy");
            var yypcy_img = document.getElementById("yypcy_img");
           // var yypcy_level = document.getElementById("yypcy_level");
           // var yypcy_serial = document.getElementById("yypcy_serial");

            //设置保育员资格证
            var byy_div = document.getElementById("byy");
            var byy_img = document.getElementById("byy_img");
            //var byy_level = document.getElementById("byy_level");
            //var byy_serial = document.getElementById("byy_serial");

            //设置母婴护理师
            var myhls_div = document.getElementById("myhls");
            var myhls_img = document.getElementById("myhls_img");
            //var myhls_serial = document.getElementById("myhls_serial");

            //设置营养师证
            var yys_div = document.getElementById("yys");
            var yys_img = document.getElementById("yys_img");
            //var yys_serial = document.getElementById("yys_serial");

            //设置护士毕业证
            var hsbyz_div = document.getElementById("hsbyz");
            var hsbyz_img = document.getElementById("hsbyz_img");
            //var hsbyz_serial = document.getElementById("hsbyz_serial");

            //设置催乳师证
            var crs_div = document.getElementById("crs");
            var crs_img = document.getElementById("crs_img");
            //var crs_serial = document.getElementById("crs_serial");

            //设置星级月嫂
            var xjys_div = document.getElementById("xjys");
            var xjys_img = document.getElementById("xjys_img");
            //var xjys_serial = document.getElementById("xjys_serial");

            //设置母婴健康指导师证
            var myjkzds_div = document.getElementById("myjkzds");
            var myjkzds_img = document.getElementById("myjkzds_img");
            //var myjkzds_serial = document.getElementById("myjkzds_serial");

            //alert skills info
            for (var i = 0; i < obj[0].skills.length; i++) {
                switch (obj[0].skills[i].name) {
                    case "育婴员资格证":
                        isExist[0] = true;
                        yyy_div.style.display = "inline-block";
                        yyy_img.src = obj[0].skills[i].img_url;
                        $scope.yyy_serial = obj[0].skills[i].sn;
                        $scope.yyy_level_select = obj[0].skills[i].level;
                        break;
                    case "家政服务员资格证":
                        isExist[1] = true;
                        jzfwy_div.style.display = "inline-block";
                        jzfwy_img.src = obj[0].skills[i].img_url;
                        $scope.jzfwy_serial = obj[0].skills[i].sn;
                        $scope.jzfwy_level_select = obj[0].skills[i].level;
                        break;
                    case "营养配餐员资格证":
                        isExist[2] = true;
                        yypcy_div.style.display = "inline-block";
                        yypcy_img.src = obj[0].skills[i].img_url;
                        $scope.yypcy_serial = obj[0].skills[i].sn;
                        $scope.yypcy_level_select = obj[0].skills[i].level;
                        break;
                    case "保育员资格证":
                        isExist[3] = true;
                        byy_div.style.display = "inline-block";
                        byy_img.src = obj[0].skills[i].img_url;
                        $scope.byy_serial = obj[0].skills[i].sn;
                        $scope.byy_level_select = obj[0].skills[i].level;
                        break;
                    case "母婴护理师":
                        isExist[4] = true;
                        myhls_div.style.display = "inline-block";
                        myhls_img.src = obj[0].skills[i].img_url;
                        $scope.myhls_serial = obj[0].skills[i].sn;
                        break;
                    case "营养师证":
                        isExist[5] = true;
                        yys_div.style.display = "inline-block";
                        yys_img.src = obj[0].skills[i].img_url;
                        $scope.yys_serial = obj[0].skills[i].sn;
                        break;
                    case "护士毕业证":
                        isExist[6] = true;
                        hsbyz_div.style.display = "inline-block";
                        hsbyz_img.src = obj[0].skills[i].img_url;
                        $scope.hsbyz_serial = obj[0].skills[i].sn;
                        break;
                    case "催乳师证":
                        isExist[7] = true;
                        crs_div.style.display = "inline-block";
                        crs_img.src = obj[0].skills[i].img_url;
                        $scope.crs_serial = obj[0].skills[i].sn;
                        break;
                    case "星级月嫂":
                        isExist[8] = true;
                        xjys_div.style.display = "inline-block";
                        xjys_img.src = obj[0].skills[i].img_url;
                        $scope.xjys_serial = obj[0].skills[i].sn;
                        break;
                    case "母婴健康指导师证":
                        isExist[9] = true;
                        myjkzds_div.style.display = "inline-block";
                        myjkzds_img.src = obj[0].skills[i].img_url;
                        $scope.myjkzds_serial = obj[0].skills[i].sn;
                        break;
                }
            }

            // alert training subjects and hospitals info
            for (var i = 0; i < obj[0].training.subjects.length; i++) {
                switch (obj[0].training.subjects[i]) {
                    case "母乳喂养培训":
                        document.getElementById("mrwypx").checked = true;
                        break;
                    case "小儿推拿培训":
                        document.getElementById("xrtnpx").checked = true;
                        break;
                    case "产后护理培训":
                        document.getElementById("chhlpx").checked = true;
                        break;
                    default:
                        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ywcpx'/><input type='text' placeholder='" + obj[0].training.subjects[i] + "' style='width:84px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
                        var template = angular.element(html);
                        var dynamicYwcpxElement = $compile(template)($scope);
                        var ywcpx_div = document.getElementById("ywcpx_div");
                        angular.element(ywcpx_div).append(dynamicYwcpxElement);
                        break;
                }
            }
            for (var i = 0; i < obj[0].training.hospital.length; i++) {
                switch (obj[0].training.hospital[i]) {
                    case "妇婴":
                        document.getElementById("fy").checked = true;
                        break;
                    case "红房子":
                        document.getElementById("hfz").checked = true;
                        break;
                    case "国妇婴":
                        document.getElementById("gfy").checked = true;
                        break;
                    default:
                        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ggyy'/><input type='text' placeholder='" + obj[0].training.hospital[i] + "' style='width:45px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
                        var template = angular.element(html);
                        var dynamicYwcpxElement = $compile(template)($scope);
                        var ggyy_div = document.getElementById("ggyy_div");
                        angular.element(ggyy_div).append(dynamicYwcpxElement);
                        break;
                }
            }

            if (obj[0].nursing.num_of_twins == 0) {
                document.getElementById("hl_w").checked = true;
            }
            else {
                document.getElementById("hl_y").checked = true;
            }

            for (var i = 0; i < obj[0].nursing.family_type.length; i++) {
                switch (obj[0].nursing.family_type[i]) {
                    case "双外籍人士":
                        document.getElementById("swjrs").checked = true;
                        break;
                    case "单外籍人士":
                        document.getElementById("dwjrs").checked = true;
                        break;
                    case "多月嫂协作":
                        document.getElementById("dysxz").checked = true;
                        break;
                    case "无母亲陪护":
                        document.getElementById("wmqpx").checked = true;
                        break;
                    default:
                        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='hljt'/><input type='text' placeholder='" + obj[0].nursing.family_type[i] + "' style='width:70px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
                        var template = angular.element(html);
                        var dynamicYwcpxElement = $compile(template)($scope);
                        var hljt_div = document.getElementById("hljt_div");
                        angular.element(hljt_div).append(dynamicYwcpxElement);
                        break;
                }
            }

            // //alert working_phote info
            for (var i = 0; i < obj[0].working_photo.length; i++) {
                var html = "<div class='material-img' style='margin-top:15px;width:260px;height:177px;'>" +
                    "<div class='certification-name' style='visibility:hidden;'><span>名称</span></div>" +
                    "<a class='upload_media' href='javascript:;' data-type='img'>" +
                    "<img class='material-photo' src='" + obj[0].working_photo[i] + "'>" +
                    "</a>" +
                    "<input type='file' accept='image/*' style='display: none'>" +
                    "<div class='tag-div'>" +
                    "<div class='tag-item'><input class='tag-input' type='text' placeholder='" + obj[0].working_photo_tag[i][0] + "'></div>" +
                    "<div class='tag-item'><input class='tag-input' type='text' placeholder='" + obj[0].working_photo_tag[i][1] + "'></div>" +
                    "<div class='tag-item'><input class='tag-input' type='text' placeholder='" + obj[0].working_photo_tag[i][2] + "'></div>" +
                    "<div class='tag-item'><input class='tag-input' type='text' placeholder='" + obj[0].working_photo_tag[i][3] + "'></div>" +
                    "</div>" +
                    "</div>";
                var template = angular.element(html);
                var dynamicYwcpxElement = $compile(template)($scope);
                var work_photo_div = document.getElementById("work_photo_div");
                angular.element(work_photo_div).append(dynamicYwcpxElement);
            }
        }
        else {
            alert(data.msg);
        }
    });

});
//设置婚姻状况为单选
function hy_yh_select() {
    if (document.getElementById("hy_wh").checked) {
        document.getElementById("hy_wh").checked = false;
    }
}
function hy_wh_select() {
    if (document.getElementById("hy_yh").checked) {
        document.getElementById("hy_yh").checked = false;
    }
}

//设置生育为单选
function sy_yy_select() {
    if (document.getElementById("sy_wy").checked) {
        document.getElementById("sy_wy").checked = false;
    }
}
function sy_wy_select() {
    if (document.getElementById("sy_yy").checked) {
        document.getElementById("sy_yy").checked = false;
    }
}

//设置学历状况为单选
function xl_xx_select() {
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_cz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_zz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_gz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_dz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
}

//设置护理经验为单选
function hl_y_select() {
    if (document.getElementById("hl_w").checked) {
        document.getElementById("hl_w").checked = false;
    }
}
function hl_w_select() {
    if (document.getElementById("hl_y").checked) {
        document.getElementById("hl_y").checked = false;
    }
}

function addMaterialImg() {
    $('div.material-img').parent().append(addMaterialImgTemplate);
    bind_upload();
}

function checkDate(start, end) {
    var startTime = start.getTime();
    var endTime = end.getTime();
    var curTime = new Date().getTime();
    return startTime<=curTime&&curTime<=endTime;
}