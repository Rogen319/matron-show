/**
 * Created by chengmenglong on 2016/8/23.
 */

/*
 * 显示用户名字
 * */
var loadBody = function(){
    document.getElementById("maternity_name").innerHTML = sessionStorage.getItem("maternity_name");
};

/*
* 登出
* */
var logout = function () {
    sessionStorage.clear();
    location.href = "login.html";
}

/*
* 将加载数据封装为一个服务
* */
var app = angular.module('myApp', []);
app.factory('loadDataService', function($http,$q){

    var service = {};

    //获取uid以及access_token
    var access_token = $.cookie("access_token");
    var uid = sessionStorage.getItem("uid");

    //获取并返回数据
    service.loadData = function(myFilter) {
        var deferred = $q.defer();
        var promise = deferred.promise;
        //返回的数据对象
        var information = new Object();

        $http({
            method:"post",
            url: ip + "yuesao/list",
            data:{
                uid:uid,
                access_token: access_token,
                page:myFilter.cur_page,
                per_page:myFilter.per_page,
                order_by:myFilter.sort_by,
                is_ascend:myFilter.is_ascend,
                filter:{
                    birth_place:myFilter.birth_place,
                    age:{
                        min:myFilter.min_age,
                        max:myFilter.max_age
                    },
                    expirence:myFilter.experience,
                    level:myFilter.level,
                    star:{
                        min:myFilter.min_star,
                        max:myFilter.max_star
                    },
                    evaluation_number:{
                        min:myFilter.min_evaluation_number,
                        max:myFilter.max_evaluation_number
                    }
                }
            }
        }).success(function(data, status, headers, config){
            if(data.success){
                //将数据存储为一个对象返回
                information.matrons = data.data.records;
                information.total_count = data.data.records_count;
                deferred.resolve(information);
            }
            else {
                alert(data.msg);
                location.href = "login.html";
            }
        });
        return promise;
    }

    //搜索并返回数据
    service.searchData = function(name,myFilter) {
        var deferred = $q.defer();
        var promise = deferred.promise;
        //返回的数据对象
        var information = new Object();

        $http({
            method:"post",
            url: ip + "yuesao/" + name + "/list",
            data:{
                uid:uid,
                access_token: access_token,
                page:myFilter.cur_page,
                per_page:myFilter.per_page
            }
        }).success(function(data, status, headers, config){
            if (data.success) {
                //将数据存储为一个对象返回
                information.matrons = data.data.records;
                information.total_count = data.data.records_count;
                deferred.resolve(information);
            }
            else{
                alert(data.msg);
            }
        });
        return promise;
    }

    return service;
});

app.controller('filterCtrl',function($scope,$http,loadDataService){

    //列表模式与头像模式的切换
    var isHeadImgModel = false;

    $scope.setImgModel = function() {
        isHeadImgModel = true;
        document.getElementById("headModel_icon").style.color = "#5cb85c";
        document.getElementById("listModel_icon").style.color = "#aaaaaa";
    }
    $scope.setListModel = function() {
        isHeadImgModel = false;
        document.getElementById("listModel_icon").style.color = "#5cb85c";
        document.getElementById("headModel_icon").style.color = "#aaaaaa";
    }
    $scope.getModel = function() {
        return isHeadImgModel;
    }

    //存储过滤信息
    var myFilter = new Object();
    myFilter.cur_page = 1;
    myFilter.per_page = 20;
    myFilter.sort_by = "mid";
    myFilter.is_ascend = true;
    myFilter.birth_place = null;
    myFilter.min_age = -1;
    myFilter.max_age = -1;
    myFilter.level = -1;

    //用于记录升序还是降序排列
    var is_id_ascend = true;
    var is_name_ascend = true;
    var is_age_ascend = true;
    var is_province_ascend = true;
    var is_level_ascend = true;

    /*
    * 搜索月嫂
    * */
    $scope.search = function(){
        var name = $scope.matron_name;
        //加载并显示数据
        $scope.matrons = loadDataService.searchData(name,myFilter).then(function(result){
            $scope.matrons = result.matrons;
            var pageNum = Math.ceil(result.total_count / myFilter.per_page);
            var pageList = [];
            for(var i = 0; i < pageNum; i++){
                pageList.push(i + 1);
            }
            $scope.pages = pageList;
        });
    }

    /*
     * 控制全部删除的出现
     * */
    $scope.isMultiSel = function() {
        var checks = document.getElementsByName("mycheckbox");
        var n = 0;
        for(var i = 0;i < checks.length; i++){
            if(checks[i].checked)
                n++;
        }
        if(n > 1){
            return true;
        }
        else{
            return false;
        }
    }

    /*
    * 跳转到添加月嫂页面
    * */
    $scope.turn2Add = function() {
        location.href = "add_matron.html";
    };

    /*
     * 跳转到月嫂信息页面
     * */
    $scope.turn2Info = function(id) {
        sessionStorage.setItem("matron_id",id);
        location.href = "matron_info.html";
    };

    /*
     * 删除id对应月嫂
     * */
    $scope.delete = function(id) {
        //获取uid以及access_token
        var access_token = $.cookie("access_token");
        var uid = sessionStorage.getItem("uid");

        $http({
            method:"post",
            url: ip + "yuesao/delete",
            data:{
                uid:uid,
                access_token: access_token,
                id:[
                    id
                ]
            }
        }).success(function(data, status, headers, config){
            if(data.success){
                alert("删除成功");
                //更新并显示数据
                $scope.matrons = loadDataService.loadData(myFilter).then(function(result){
                    $scope.matrons = result.matrons;
                    var pageNum = Math.ceil(result.total_count / myFilter.per_page);
                    var pageList = [];
                    for(var i = 0; i < pageNum; i++){
                        pageList.push(i + 1);
                    }
                    $scope.pages = pageList;
                });
            }
            else {
                alert(data.msg);
            }
        });
    }

    /*
     * 批量删除月嫂
     * */
    $scope.deleteAll = function() {
        //获取uid以及access_token
        var access_token = $.cookie("access_token");
        var uid = sessionStorage.getItem("uid");

        //待删除id列表
        var id_delete = new Array();

        //获取月嫂id列表
        var checks = document.getElementsByName("mycheckbox");
        for(var i = 0;i < checks.length; i++){
            if(checks[i].checked){
                id_delete.push(checks[i].parentNode.nextSibling.nextSibling.innerText);
            }
        }

        alert(id_delete);

        $http({
            method:"post",
            url: ip + "yuesao/delete",
            data:{
                uid:uid,
                access_token: access_token,
                id:id_delete
            }
        }).success(function(data, status, headers, config){
            if(data.success){
                alert("删除成功");

                //更新并显示数据
                $scope.matrons = loadDataService.loadData(myFilter).then(function(result){
                    $scope.matrons = result.matrons;
                    var pageNum = Math.ceil(result.total_count / myFilter.per_page);
                    var pageList = [];
                    for(var i = 0; i < pageNum; i++){
                        pageList.push(i + 1);
                    }
                    $scope.pages = pageList;
                });

            }
            else {
                alert(data.msg);
            }
        });
    }

    /*
    * 增加减少工作经验以及星级
    * */
    $scope.add_exp = function() {
        var experience = parseInt(document.getElementById("experience").innerHTML);
        if(experience <= 9){
            experience = experience + 1;
            document.getElementById("experience").innerHTML = experience;
        }
        else {
            alert("最高可选工作经验为10年！");
        }
    };
    $scope.minus_exp = function() {
        var experience = document.getElementById("experience").innerHTML;
        if(experience >= 2){
            experience -= 1;
            document.getElementById("experience").innerHTML = experience;
        }
        else {
            alert("最低可选工作经验为1年！");
        }
    };
    $scope.add_star = function() {
        var level = parseInt(document.getElementById("level").innerHTML);
        if(level <= 4){
            level = level + 1;
            document.getElementById("level").innerHTML = level;
        }
        else {
            alert("最高可选星级为5星！");
        }
    };
    $scope.minus_star = function() {
        var level = document.getElementById("level").innerHTML;
        if(level >= 2){
            level -= 1;
            document.getElementById("level").innerHTML = level;
        }
        else {
            alert("最低可选星级为一星！");
        }
    };

    $scope.show_filter = false;//默认不显示筛选栏

    //获取uid以及access_token
    var access_token = $.cookie("access_token");
    var uid = sessionStorage.getItem("uid");

    //加载并显示数据
    $scope.matrons = loadDataService.loadData(myFilter).then(function(result){
        $scope.matrons = result.matrons;
        var pageNum = Math.ceil(result.total_count / myFilter.per_page);
        var pageList = [];
        for(var i = 0; i < pageNum; i++){
            pageList.push(i + 1);
        }
        $scope.pages = pageList;
    });

    //设置排序方式
    $scope.sort_by_id = function(){
        myFilter.is_ascend = is_id_ascend;
        myFilter.sort_by = "mid";

        //更新并显示数据
        $scope.matrons = loadDataService.loadData(myFilter).then(function(result){
            $scope.matrons = result.matrons;
            var pageNum = Math.ceil(result.total_count / myFilter.per_page);
            var pageList = [];
            for(var i = 0; i < pageNum; i++){
                pageList.push(i + 1);
            }
            $scope.pages = pageList;
        });
        is_id_ascend = !is_id_ascend;
    };
    $scope.sort_by_name = function(){
        myFilter.is_ascend = is_name_ascend;
        myFilter.sort_by = "name";

        //更新并显示数据
        $scope.matrons = loadDataService.loadData(myFilter).then(function(result){
            $scope.matrons = result.matrons;
            var pageNum = Math.ceil(result.total_count / myFilter.per_page);
            var pageList = [];
            for(var i = 0; i < pageNum; i++){
                pageList.push(i + 1);
            }
            $scope.pages = pageList;
        });

        is_name_ascend = !is_name_ascend;
    };
    $scope.sort_by_age = function(){
        myFilter.is_ascend = is_age_ascend;
        myFilter.sort_by = "age";

        //更新并显示数据
        $scope.matrons = loadDataService.loadData(myFilter).then(function(result){
            $scope.matrons = result.matrons;
            var pageNum = Math.ceil(result.total_count / myFilter.per_page);
            var pageList = [];
            for(var i = 0; i < pageNum; i++){
                pageList.push(i + 1);
            }
            $scope.pages = pageList;
        });

        is_age_ascend = !is_age_ascend;
    };
    $scope.sort_by_province = function(){
        myFilter.is_ascend = is_province_ascend;
        myFilter.sort_by = "native_place";

        //更新并显示数据
        $scope.matrons = loadDataService.loadData(myFilter).then(function(result){
            $scope.matrons = result.matrons;
            var pageNum = Math.ceil(result.total_count / myFilter.per_page);
            var pageList = [];
            for(var i = 0; i < pageNum; i++){
                pageList.push(i + 1);
            }
            $scope.pages = pageList;
        });

        is_province_ascend = !is_province_ascend;
    };
    $scope.sort_by_level = function(){
        myFilter.is_ascend = is_level_ascend;
        myFilter.sort_by = "level";

        //更新并显示数据
        $scope.matrons = loadDataService.loadData(myFilter).then(function(result){
            $scope.matrons = result.matrons;
            var pageNum = Math.ceil(result.total_count / myFilter.per_page);
            var pageList = [];
            for(var i = 0; i < pageNum; i++){
                pageList.push(i + 1);
            }
            $scope.pages = pageList;
        });
        is_level_ascend = !is_level_ascend;
    };

    //设置当前选中页样式
    $scope.isActivePage = function (page) {
        return myFilter.cur_page == page;
    };

    //上一页
    $scope.previous = function () {
        $scope.selectPage(myFilter.cur_page - 1);
    }

    //下一页
    $scope.next = function () {
        $scope.selectPage(myFilter.cur_page + 1);
    };

    //选择页面
    $scope.selectPage = function(page) {
        myFilter.cur_page = page;
        //更新并显示数据
        $scope.matrons = loadDataService.loadData(myFilter).then(function(result){
            $scope.matrons = result.matrons;
            var pageNum = Math.ceil(result.total_count / myFilter.per_page);
            var pageList = [];
            for(var i = 0; i < pageNum; i++){
                pageList.push(i + 1);
            }
            $scope.pages = pageList;
        });
    }

    $scope.isFirstSelected = true;
    $scope.isSecondSelected = false;
    $scope.isThirdSelected = false;

    /*
    *选择每页显示的数据
    * */
    $scope.changePerPage = function(number) {
        myFilter.per_page = number;
        myFilter.cur_page = 1;

        //更新显示数据
        $scope.matrons = loadDataService.loadData(myFilter).then(function(result){
            $scope.matrons = result.matrons;
            var pageNum = Math.ceil(result.total_count / myFilter.per_page);
            var pageList = [];
            for(var i = 0; i < pageNum; i++){
                pageList.push(i + 1);
            }
            $scope.pages = pageList;
        });

        if(number == 20){
            $scope.isFirstSelected = true;
            $scope.isSecondSelected = false;
            $scope.isThirdSelected = false;
        }
        else if(number == 50){
            $scope.isFirstSelected = false;
            $scope.isSecondSelected = true;
            $scope.isThirdSelected = false;
        }
        else {
            $scope.isFirstSelected = false;
            $scope.isSecondSelected = false;
            $scope.isThirdSelected = true;
        }
    }

    /*
    * 控制删除的出现：选中复选框以及鼠标悬浮
    * */
    $scope.myClick = function(x) {
        if(!x.isSelected){
            x.show = true;
            x.isSelected = true;
        }
        else {
            x.isSelected = false;
            x.show = false;
        }
    };

    $scope.myHover = function(x){
        if(!x.isSelected){
            x.show = true;
        }
    };
    $scope.myLeave = function(x){
        if(!x.isSelected){
            x.show = false;
        }
    };

    /*
    * 控制筛选栏的展开与收起
    * */
    $scope.control_filter = function() {
        if($scope.show_filter){
            $scope.show_filter = false;
            document.getElementById("filter_icon").style.color = "#aaaaaa";
        }
        else {
            $scope.show_filter = true;
            document.getElementById("filter_icon").style.color = "#5cb85c";
        }
    }

    /*
    * 筛选实现
    * */
    $scope.searchByFilter = function() {
        cur_page = 1;
        //获取uid以及access_token
        var access_token = $.cookie("access_token");
        var uid = sessionStorage.getItem("uid");

        var selectedVal = document.getElementById('province');
        var province = selectedVal.value;
        var min_age = $scope.min_age;
        var max_age = $scope.max_age;
        if((min_age == "") || (min_age == null)){
            min_age = 1;
        }
        if((max_age == "") || (max_age == null)){
            max_age = 100;
        }
        var experience = document.getElementById("experience").innerHTML;
        var level = document.getElementById("level").innerHTML;
        var min_star = $scope.min_star;
        var max_star = $scope.max_star;
        var min_evaluation_number = $scope.min_evaluation_number;
        var max_evaluation_number = $scope.max_evaluation_number;

        myFilter.birth_place = province;
        myFilter.min_age = min_age;
        myFilter.max_age = max_age;
        myFilter.experience = experience;
        myFilter.level = level;
        myFilter.min_star = min_star;
        myFilter.max_star = max_star;
        myFilter.min_evaluation_number = min_evaluation_number;
        myFilter.max_evaluation_number = max_evaluation_number;

        //更新显示筛选的数据
        $scope.matrons = loadDataService.loadData(myFilter).then(function(result){
            $scope.matrons = result.matrons;
            var pageNum = Math.ceil(result.records_count / myFilter.per_page);
            var pageList = [];
            for(var i = 0; i < pageNum; i++){
                pageList.push(i + 1);
            }
            $scope.pages = pageList;
        });
    };
});

