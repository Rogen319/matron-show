/**
 * Created by 文海 on 2016/9/7.
 */
var routerApp = angular.module('myApp', ['ui.router']);

var routerConfig = routerApp.config(['$stateProvider', function($stateProvider) {

    $stateProvider

        .state('overview_page', {
            url: '../overview_page',
            templateUrl: '../overview_page.html',
            controller: 'filterCtrl'
        })
        .state('matron_info', {
            url: '/matron_info/:id',
            templateUrl: '^/matron_info.html',
            controller: 'infoCtrl'
        })

}]);
