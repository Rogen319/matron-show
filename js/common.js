$().ready(function() {
	$("#login_form").validate({
		rules: {
			username: "required",
			password: {
				required: true,
				minlength: 6
			},
		},
		messages: {
			username: "请输入姓名",
			password: {
				required: "请输入密码",
				minlength: jQuery.format("密码不能小于{0}个字符")
			},
		}
	});
	$("#modify_form").validate({
		rules: {
			old_password: "required",
			password: {
				required: true,
				minlength: 5
			},
			rpassword: {
				required: true,
				equalTo: "#new_password"
			}
		},
		messages: {
			old_password: "请输入原密码",
			password: {
				required: "请输入新密码",
				minlength: jQuery.format("密码不能小于{0}个字符")
			},
			rpassword: {
				required: "请再次输入新密码",
				equalTo: "两次密码不一样"
			}
		}
	});
});