/**
 * Created by zzt on 16-9-7.
 */
function bind_upload() {
    if (!(window.File || window.FileReader || window.FileList || window.Blob)) {
        alert('请勿使用低版本的IE浏览器进行访问');
        return;
    }
    $('.upload_media').each(function () {
        var a = $(this);
        var input = a.next();
        var media = a.children();
        var filename;
        var md5;
        a.click(function () {
            input.click();
        });
        input.change(function () {
            var file = input.prop('files')[0];
            // media.attr('src', getFileUrl(file));
            // console.log(getFileUrl(file));
            if (file == null)
                return;
            if (file.size > 20971520) {
                alert('文件过大');
                return;
            }
            var chunkSize = 2097152;
            var chunks = Math.ceil(file.size / chunkSize);
            var currentChunk = 0;
            var blobSlice = File.prototype.mozSlice || File.prototype.webkitSlice || File.prototype.slice;
            var spark = new SparkMD5();

            var fileReader = new FileReader();

            fileReader.onload = function (e) {
                console.log("读取文件", currentChunk + 1, "/", chunks);
                spark.appendBinary(e.target.result);
                currentChunk++;

                if (currentChunk < chunks) {
                    loadNext();
                } else {
                    console.log("finished loading");
                    md5 = spark.end().toUpperCase();
                    console.info("计算的Hash", md5);

                    var param = getPostPolicy();

                    postFile(param);
                }
            };

            function loadNext() {
                var start = currentChunk * chunkSize, end = start + chunkSize >= file.size ? file.size : start + chunkSize;

                fileReader.readAsBinaryString(blobSlice.call(file, start, end));
            }

            loadNext();

            function getPostPolicy() {
                var ret;
                $.ajax({
                    url: './api/media/getPostPolicy/' + a.attr('data-type'),
                    type: 'GET',
                    async: false,
                    success: function (data) {
                        console.log(data);
                        ret = eval(data);

                        console.log(ret);
                    },
                    dataType: 'json'
                });
                return ret;
            }

            function postFile(param) {
                var formData = new FormData();
                var filename = file.name;
                var key = param.dir + "/" + md5 + filename.substring(filename.lastIndexOf('.'));
                console.log("object key: " + key);
                formData.append('key', key);
                formData.append('policy', param.policy);
                formData.append('OSSAccessKeyId', param.accessid);
                formData.append('Signature', param.signature);
                formData.append('x-oss-object-acl', 'public-read');
                formData.append('file', file);

                var file_url = param.host + '/' + key;
                var onUploaded = eval(a.attr('data-on-uploaded'));
                a.click(null);
                $.ajax({
                    url: param.host,
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function () {
                        console.log('file_url: ' + file_url);
                        a.attr('data-file-url', file_url);
                        media.attr('src', file_url);
                        media.attr('data-url', file_url);
                        a.click(function () {
                            input.click();
                        });
                        onUploaded.call();
                    },
                    error: function () {
                        console.log("failed");
                        media.attr('src', file_url);
                        media.attr('data-url', file_url);
                        a.click(function () {
                            input.click();
                        });
                        onUploaded.call();
                    }
                });
            }

            function getFileUrl(file) {
                var url = null;
                if (window.createObjectURL != undefined) {
                    url = window.createObjectURL(file)
                } else if (window.URL != undefined) {
                    url = window.URL.createObjectURL(file)
                } else if (window.webkitURL != undefined) {
                    url = window.webkitURL.createObjectURL(file)
                }
                return url
            }
        });
    })
}
