/**
 * Created by lwh on 2016/8/30.
 */

/*
 * 显示用户名字
 * */
var loadBody = function(){
    var username = sessionStorage.getItem("maternity_name");
    if(username == null){
        alert("请先登录");
        location.href = "login.html";
    }
    else{
        document.getElementById("maternity_name").innerHTML = sessionStorage.getItem("maternity_name");
    }
};

/*
 * 登出
 * */
var logout = function () {
    sessionStorage.clear();
    location.href = "login.html";
}

/*
* 加载id对应的月嫂信息
* */
var app = angular.module('myApp', []);
app.controller('infoCtrl', function($scope,$compile,$http) {

    //记录证书是否存在，默认不存在
    var isExist = new Array(10);
    for(var i = 0; i < 10; i++){
        isExist[i] = false;
    }

    $scope.setNoneDisplay = function() {
        document.getElementById('yyy').style.display = "none";
        document.getElementById('jzfwy').style.display = "none";
        document.getElementById('yypcy').style.display = "none";
        document.getElementById('byy').style.display = "none";
        document.getElementById('myhls').style.display = "none";
        document.getElementById('yys').style.display = "none";
        document.getElementById('hsbyz').style.display = "none";
        document.getElementById('crs').style.display = "none";
        document.getElementById('xjys').style.display = "none";
        document.getElementById('myjkzds').style.display = "none";
    }
    $scope.setAllDisplay = function() {
        if(isExist[0] == true){
            document.getElementById('yyy').style.display = "inline-block";
        }
        if(isExist[1] == true){
            document.getElementById('jzfwy').style.display = "inline-block";
        }
        if(isExist[2] == true){
            document.getElementById('yypcy').style.display = "inline-block";
        }
        if(isExist[3] == true){
            document.getElementById('byy').style.display = "inline-block";
        }
        if(isExist[4] == true){
            document.getElementById('myhls').style.display = "inline-block";
        }
        if(isExist[5] == true){
            document.getElementById('yys').style.display = "inline-block";
        }
        if(isExist[6] == true){
            document.getElementById('hsbyz').style.display = "inline-block";
        }
        if(isExist[7] == true){
            document.getElementById('crs').style.display = "inline-block";
        }
        if(isExist[8] == true){
            document.getElementById('xjys').style.display = "inline-block";
        }
        if(isExist[9] == true){
            document.getElementById('myjkzds').style.display = "inline-block";
        }
    }

    $scope.setNoneDisplay();
    $scope.setSelectedCertification = function(x) {
        $scope.setNoneDisplay();
        if(x == "myhls") {
            if(isExist[4]){
                document.getElementById('myhls').style.display = "block";
            }
            else{
                alert("该月嫂未上传此证书！");
            }
        }
        else if(x == "yyy"){
            if(isExist[0] == true){
                document.getElementById('yyy').style.display = "block";
            }
            else{
                alert("该月嫂未上传此证书！");
            }
        }
        else if(x == "jzfwy"){
            if(isExist[1] == true){
                document.getElementById('jzfwy').style.display = "block";
            }
            else{
                alert("该月嫂未上传此证书！");
            }
        }
        else if(x == "yypcy"){
            if(isExist[2] == true){
                document.getElementById('yypcy').style.display = "block";
            }
            else{
                alert("该月嫂未上传此证书！");
            }
        }
        else if(x == "byy"){
            if(isExist[3] == true){
                document.getElementById('byy').style.display = "block";
            }
            else{
                alert("该月嫂未上传此证书！");
            }
        }
        else if(x == "hsbyz"){
            if(isExist[6] == true){
                document.getElementById('hsbyz').style.display = "block";
            }
            else{
                alert("该月嫂未上传此证书！");
            }
        }
        else if(x == "crs"){
            if(isExist[7] == true){
                document.getElementById('crs').style.display = "block";
            }
            else{
                alert("该月嫂未上传此证书！");
            }
        }
        else if(x == "xjys"){
            if(isExist[8] == true){
                document.getElementById('xjys').style.display = "block";
            }
            else{
                alert("该月嫂未上传此证书！");
            }
        }
        else if(x == "yys"){
            if(isExist[5] == true){
                document.getElementById('yys').style.display = "block";
            }
            else{
                alert("该月嫂未上传此证书！");
            }
        }
        else if(x == "myjkzds"){
            if(isExist[9] == true){
                document.getElementById('myjkzds').style.display = "block";
            }
            else{
                alert("该月嫂未上传此证书！");
            }
        }
        else{
            $scope.setAllDisplay();
        }
    }


    var id = sessionStorage.getItem("matron_id");
    //获取uid以及access_token
    var access_token = $.cookie("access_token");
    var uid = sessionStorage.getItem("uid");
    var url = ip + "yuesao/" + id + "/detail";
    //修改月嫂信息
    $scope.modifyMatron = function() {
        location.href = "modify_matron.html";
    };

    //获取月嫂信息
    $http({

        method:"post",
        url:url,
        data:{
            uid:uid,
            access_token:access_token
        }
    }).success(function(data, status, headers, config){
        if(data.success){
            var head_img = document.getElementById("head_img");
            head_img.src = data.data.head_img_url;
            var intro_video = document.getElementById("intro_video");
            intro_video.src = data.data.intro_video_url;
            //get certificates json array, obj[0] is json object
            var obj = eval('(' + data.data.certificates + ')');
            var id_front_img = document.getElementById("id_front_img");
            id_front_img.src = obj[0].id_card.front_url;
            var id_back_img = document.getElementById("id_back_img");
            id_back_img.src = obj[0].id_card.back_url;

            var health_img = document.getElementById("health_img");
            health_img.src = obj[0].health.img_url;

            //设置育婴员资格证
            var yyy_div = document.getElementById("yyy");
            var yyy_img = document.getElementById("yyy_img");
            var yyy_level = document.getElementById("yyy_level");
            var yyy_serial = document.getElementById("yyy_serial");

            //设置家政服务员资格证
            var jzfwy_div = document.getElementById("jzfwy");
            var jzfwy_img = document.getElementById("jzfwy_img");
            var jzfwy_level = document.getElementById("jzfwy_level");
            var jzfwy_serial = document.getElementById("jzfwy_serial");

            //设置营养配餐员资格证
            var yypcy_div = document.getElementById("yypcy");
            var yypcy_img = document.getElementById("yypcy_img");
            var yypcy_level = document.getElementById("yypcy_level");
            var yypcy_serial = document.getElementById("yypcy_serial");

            //设置保育员资格证
            var byy_div = document.getElementById("byy");
            var byy_img = document.getElementById("byy_img");
            var byy_level = document.getElementById("byy_level");
            var byy_serial = document.getElementById("byy_serial");

            //设置母婴护理师
            var myhls_div = document.getElementById("myhls");
            var myhls_img = document.getElementById("myhls_img");
            var myhls_serial = document.getElementById("myhls_serial");

            //设置营养师证
            var yys_div = document.getElementById("yys");
            var yys_img = document.getElementById("yys_img");
            var yys_serial = document.getElementById("yys_serial");

            //设置护士毕业证
            var hsbyz_div = document.getElementById("hsbyz");
            var hsbyz_img = document.getElementById("hsbyz_img");
            var hsbyz_serial = document.getElementById("hsbyz_serial");

            //设置催乳师证
            var crs_div = document.getElementById("crs");
            var crs_img = document.getElementById("crs_img");
            var crs_serial = document.getElementById("crs_serial");

            //设置星级月嫂
            var xjys_div = document.getElementById("xjys");
            var xjys_img = document.getElementById("xjys_img");
            var xjys_serial = document.getElementById("xjys_serial");

            //设置母婴健康指导师证
            var myjkzds_div = document.getElementById("myjkzds");
            var myjkzds_img = document.getElementById("myjkzds_img");
            var myjkzds_serial = document.getElementById("myjkzds_serial");

            //alert skills info
            for(var i = 0; i <obj[0].skills.length; i++){
                switch (obj[0].skills[i].name){
                    case "育婴员资格证":
                        isExist[0] = true;
                        yyy_div.style.display = "inline-block";
                        yyy_img.src = obj[0].skills[i].img_url;
                        yyy_level.innerHTML = "5";
                        break;
                    case "家政服务员资格证":
                        isExist[1] = true;
                        jzfwy_div.style.display = "inline-block";
                        jzfwy_img.src = obj[0].skills[i].img_url;
                        jzfwy_level.innerHTML = "5";
                        break;
                    case "营养配餐员资格证":
                        isExist[2] = true;
                        yypcy_div.style.display = "inline-block";
                        yypcy_img.src = obj[0].skills[i].img_url;
                        yypcy_level.innerHTML = "5";
                        break;
                    case "保育员资格证":
                        isExist[3] = true;
                        byy_div.style.display = "inline-block";
                        byy_img.src = obj[0].skills[i].img_url;
                        byy_level.innerHTML = "5";
                        break;
                    case "母婴护理师":
                        isExist[4] = true;
                        myhls_div.style.display = "inline-block";
                        myhls_img.src = obj[0].skills[i].img_url;
                        break;
                    case "营养师证":
                        isExist[5] = true;
                        yys_div.style.display = "inline-block";
                        yys_img.src = obj[0].skills[i].img_url;
                        break;
                    case "护士毕业证":
                        isExist[6] = true;
                        hsbyz_div.style.display = "inline-block";
                        hsbyz_img.src = obj[0].skills[i].img_url;
                        break;
                    case "催乳师证":
                        isExist[7] = true;
                        crs_div.style.display = "inline-block";
                        crs_img.src = obj[0].skills[i].img_url;
                        break;
                    case "星级月嫂":
                        isExist[8] = true;
                        xjys_div.style.display = "inline-block";
                        xjys_img.src = obj[0].skills[i].img_url;
                        break;
                    case "母婴健康指导师证":
                        isExist[9] = true;
                        myjkzds_div.style.display = "inline-block";
                        myjkzds_img.src = obj[0].skills[i].img_url;
                        break;
                }
            }

            // alert training subjects and hospitals info
            for(var i = 0; i <obj[0].training.subjects.length; i++){
                var html;
                if(i == 0){
                    html = "<div class='display-checkbox-item' style='margin-left:10px;'> <span>" + obj[0].training.subjects[i] + "</span> </div>";
                }
                else {
                    html = "<span>、</span><div class='display-checkbox-item'> <span>" + obj[0].training.subjects[i] + "</span> </div>";
                }
                var template = angular.element(html);
                var dynamicYwcpxElement = $compile(template)($scope);
                var ywcpx_div = document.getElementById("ywcpx_div");
                angular.element(ywcpx_div).append(dynamicYwcpxElement);
            }
            for(var i = 0; i <obj[0].training.hospital.length; i++){
                var html;
                if(i == 0){
                    html = "<div class='display-checkbox-item' style='margin-left:10px;'> <span>" + obj[0].training.hospital[i] + "</span> </div>";
                }
                else {
                    html = "<span>、</span><div class='display-checkbox-item'> <span>" + obj[0].training.hospital[i] + "</span> </div>";
                }
                var template = angular.element(html);
                var dynamicYwcpxElement = $compile(template)($scope);
                var ggyy_div = document.getElementById("ggyy_div");
                angular.element(ggyy_div).append(dynamicYwcpxElement);
            }

            for(var i = 0; i <obj[0].nursing.family_type.length; i++){
                var html;
                if(i == 0){
                    html = "<div class='display-checkbox-item' style='margin-left:10px;'> <span>" + obj[0].nursing.family_type[i] + "</span> </div>";
                }
                else {
                    html = "<span>、</span><div class='display-checkbox-item'> <span>" + obj[0].nursing.family_type[i] + "</span> </div>";
                }
                var template = angular.element(html);
                var dynamicYwcpxElement = $compile(template)($scope);
                var hljt_div = document.getElementById("hljt_div");
                angular.element(hljt_div).append(dynamicYwcpxElement);
                var sbtjy = document.getElementById("sbtjy");
            }
            //add nursing double body
            if(obj[0].nursing.num_of_twins){
                sbtjy.innerHTML = "有";
            }else {
                sbtjy.innerHTML = "无";
            }
            
            // //alert working_phote info
            for(var i = 0; i <obj[0].working_photo.length; i++){
                var html = "<div class='material-img'> <div class='certification-name'><span>名称</span></div> <div><img src='" +obj[0].working_photo[i] + "'></div> <div class='tag-div'><div class='tag-item'><span>" + obj[0].working_photo_tag[i][0] + "</span></div><div class='tag-item'><span>" + obj[0].working_photo_tag[i][1] + "</span></div><div class='tag-item'><span>" + obj[0].working_photo_tag[i][2] + "</span></div><div class='tag-item'><span>" + obj[0].working_photo_tag[i][3] + "</span></div></div></div>";
                var template = angular.element(html);
                var dynamicYwcpxElement = $compile(template)($scope);
                var work_photo_div = document.getElementById("work_photo_div");
                angular.element(work_photo_div).append(dynamicYwcpxElement);
            }

            document.getElementById("name").innerHTML = data.data.name;
            document.getElementById("age").innerHTML = data.data.age;
            document.getElementById("birth_place").innerHTML = data.data.birth_place;
            document.getElementById("five_stars").innerHTML = data.data.grade.five;
            document.getElementById("four_stars").innerHTML = data.data.grade.four;
            document.getElementById("three_stars").innerHTML = data.data.grade.three;
            document.getElementById("two_stars").innerHTML = data.data.grade.two;
            document.getElementById("one_star").innerHTML = data.data.grade.one;
            document.getElementById("phone").innerHTML = data.data.phone_num;
            document.getElementById("id_num").innerHTML = obj[0].id_card.num;
            var health_start = new Date(obj[0].health.validity_period.start);
            document.getElementById("health_start").innerHTML = health_start.getFullYear() + "-" + (health_start.getMonth() + 1) + "-" + health_start.getDate();
            var health_end = new Date(obj[0].health.validity_period.end);
            document.getElementById("health_end").innerHTML = health_end.getFullYear() + "-" + (health_end.getMonth() + 1) + "-" + health_end.getDate();
            var first_time_date = new Date(obj[0].nursing.first_time);
            document.getElementById("hlsj").innerHTML = first_time_date.getFullYear() + "-" + (first_time_date.getMonth() + 1) + "-" + first_time_date.getDate();
            document.getElementById("jtsl").innerHTML = obj[0].nursing.num_of_family;
        }
        else {
            alert(data.msg);
        }
    });

});
