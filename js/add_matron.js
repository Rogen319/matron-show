/**
 * Created by lwh on 2016/8/30.
 */

/*
 * 显示用户名字
 * */
var addMaterialImgTemplate;
var loadBody = function () {
    var username = sessionStorage.getItem("maternity_name");
    if (username == null) {
        alert("请先登录");
        location.href = "login.html";
    }
    else {
        document.getElementById("maternity_name").innerHTML = sessionStorage.getItem("maternity_name");
    }
    // 绑定上传配置
    bind_upload();
    addMaterialImgTemplate = $('div.material-img:last').prop('outerHTML');
    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);
    $('.date-picker').datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true
    });
};

/*
 * 登出
 * */
var logout = function () {
    sessionStorage.clear();
    location.href = "login.html";
}

var app = angular.module('myApp', []);
app.controller('myCtrl', function ($scope, $compile, $http) {
    var hy = "";
    var sy = "";
    var xl = "";
    var hljy = "";
    var yy = new Array();
    var ywcpx = new Array();
    var ggyy = new Array();
    var hljt = new Array();

    $scope.addMatron = function () {
        //获取uid以及access_token
        var access_token = $.cookie("access_token");
        var uid = sessionStorage.getItem("uid");

        var name = $scope.name;
        var age = $scope.age;
        var province = document.getElementById("province");
        var index = province.selectedIndex;
        var birth_place = province.options[index].value;
        var phone = $scope.phone_num;
        var emergency = $scope.emergency;
        var relation = $scope.relation;
        var contact = $scope.contact;
        var id_num = $scope.id_num;
        var health_start = $scope.health_start;
        var health_end = $scope.health_end;
        var hlsj = new Date($scope.hlsj);
        var jtsl = $scope.jtsl;
        var health_start = new Date($scope.health_start);
        var health_end = new Date($scope.health_end);

        //婚姻状况
        var obj = document.getElementsByName("hyzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                hy = obj[i].nextSibling.innerText;
            }
        }

        //生育状况
        obj = document.getElementsByName("syzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                sy = obj[i].nextSibling.innerText;
            }
        }

        //学历状况
        obj = document.getElementsByName("xlzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                xl = obj[i].nextSibling.innerText;
            }
        }

        //语言能力
        obj = document.getElementsByName("yynl");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                yy.push(obj[i].nextSibling.innerText);
            }
        }

        //已完成培训
        obj = document.getElementsByName("ywcpx");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                var temp;
                if (obj[i].checked) {
                    temp = obj[i].nextSibling;
                    if (temp.nodeName == "SPAN") {
                        ywcpx.push(obj[i].nextSibling.innerText);
                    }
                    else {
                        ywcpx.push(obj[i].nextSibling.value);
                    }
                }
            }
        }

        //跟岗医院
        obj = document.getElementsByName("ggyy");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                var temp;
                if (obj[i].checked) {
                    temp = obj[i].nextSibling;
                    if (temp.nodeName == "SPAN") {
                        ggyy.push(obj[i].nextSibling.innerText);
                    }
                    else {
                        ggyy.push(obj[i].nextSibling.value);
                    }
                }
            }
        }

        //护理双胞胎经验
        obj = document.getElementsByName("hljy");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                hljy = obj[i].nextSibling.innerText;
            }
        }

        //护理家庭类型
        obj = document.getElementsByName("hljt");
        for (var i = 0; i < obj.length; i++) {
            var temp;
            if (obj[i].checked) {
                temp = obj[i].nextSibling;
                if (temp.nodeName == "SPAN") {
                    hljt.push(obj[i].nextSibling.innerText);
                }
                else {
                    hljt.push(obj[i].nextSibling.value);
                }
            }
        }

        var head_img_url = $('img.head-img').attr('data-url');
        var intro_video_url = $('video.intro-video').attr('data-url');
        var id_front_img_url = $('img.front_id_img').attr('data-url');
        var id_back_img_url = $('img.back_id_img').attr('data-url');
        var health_img_url = $('img.health_img').attr('data-url');
        var skills = [];
        $('.certification-img:visible').each(function () {
            var skill = {};
            skill.name = $(this).children('.certification-name').children('span').text();
            var rank_select = $(this).find('rank-select');
            if (rank_select != null) {
                skill.level = parseInt(rank_select.val());
            }
            skill.num = $(this).find('.certification-num').val();
            skill.img_url = $(this).find('img.skill-img').attr('data-url');
            skills.push(skill);
        });

        var working_photo = [];
        $('img.material-photo').each(function () {
            working_photo.push($(this).attr('data-url'));
        });
        var working_photo_tag = [];
        if(typeof($scope.tag1) != "undefined"){
            working_photo_tag.push($scope.tag1);
        }
        if(typeof($scope.tag2) != "undefined"){
            working_photo_tag.push($scope.tag2);
        }
        if(typeof($scope.tag3) != "undefined"){
            working_photo_tag.push($scope.tag3);
        }
        if(typeof($scope.tag4) != "undefined"){
            working_photo_tag.push($scope.tag4);
        }

        // 判断输入合法性
        if (!/\d{1,2}/.test(age) || age > 80 || age < 18) {
            alert("请填写正确的年龄");
            return;
        }

        if (!/\d{11}/.test(phone)) {
            alert("请输入11位手机号码");
            return;
        }

        if (!/\d{11}/.test(contact)) {
            alert("请输入11位手机号码");
            return;
        }

        if (!/\d{17}(\d|x)/.test($scope.id_num)) {
            alert("请输入正确的身份证号");
            return;
        }

        if (!checkDate(health_start,health_end)) {
            alert("健康证有效期无效");
            return;
        }

        {
            var curTime = new Date().getTime();
            var hlsjTime = hlsj.getTime();
            if (curTime<hlsjTime) {
                alert("最早护理时间无效");
                return;
            }
        }

        if (!/\d{1,2}/.test(jtsl) || jtsl < 0 || jtsl >99) {
            alert('护理家庭数量不合法，请输入0~99之间的数字');
            return;
        }

        if (name.length>20) {
            alert("姓名过长，最多20字符");
            return;
        }
        if (emergency.length>20) {
            alert("紧急联系人姓名过长，最多20字符");
            return;
        }
        if (relation.length>20) {
            alert("与紧急联系人关系过长，最多20字符");
            return;
        }

        //通过http添加月嫂
        $http({

            method: "post",
            url: ip + "yuesao/detail",
            data: {
                uid: uid,
                access_token: access_token,
                name: name, // 月嫂姓名
                age: age, // 月嫂年龄
                birth_place: birth_place, // 月嫂籍贯
                phone_num: phone, // 月嫂联系电话
                head_img_url: head_img_url, // 月嫂头像地址
                intro_video_url: intro_video_url, // 月嫂介绍视频地址
                educationBackground: xl,
                emergencyContactPersonName: emergency,
                emergencyContactPersonRelation: relation,
                emergencyContactPersonNumber: contact,
                procreationState: sy,
                marriageState: hy,
                language: yy,
                level: 5,//页面中没有相关选项
                grade: { // 月嫂评分//页面中没有相关选项 其中各项的值代表该评分的数量
                    one: 1,
                    two: 2,
                    three: 3,
                    four: 4,
                    five: 5
                },
                certificates: { // 证书资料
                    id_card: { // 身份证
                        num: $scope.id_num, // 证号
                        front_url: id_front_img_url, // 正面图片地址
                        back_url: id_back_img_url // 背面图片地址
                    },
                    health: { // 健康证
                        validity_period: { // 有效期
                            start: $scope.health_start, // 起始时间
                            end: $scope.health_end // 失效时间
                        },
                        img_url: head_img_url // 证件图片地址
                    },
                    skills: skills, // 技能证书
                    training: {
                        subjects: ywcpx, // 已完成培训项目，催乳师培训等
                        hospital: ggyy // 跟岗医院
                    },
                    nursing: {
                        first_time: $scope.hlsj, // 最早护理时间
                        num_of_family: jtsl, // 护理家庭数量
                        num_of_twins: hljy, // 有无护理双胞胎经验
                        family_type: // 家庭类型 双外籍、单外籍等
                        hljt,
                        location: []
                    },
                    working_photo: working_photo,// 工作照链接地址
                    working_photo_tag:working_photo_tag
                }
            }
        }).success(function (data, status, headers, config) {
            if (data.success) {
                alert("success");
                location.href = "overview_page.html"
            }
            else {
                alert(data.msg);
            }
        });
    };

    $scope.addIDImg = function () {
        document.getElementById('id1-img-input').click();
    };
    $scope.addHealthImg = function () {
        document.getElementById('health-img-input').click();
    };
    $scope.add_ywcpx = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ywcpx'/><input type='text' placeholder='自定义' style='width:84px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var ywcpx_div = document.getElementById("ywcpx_div");
        angular.element(ywcpx_div).append(dynamicYwcpxElement);
    }

    $scope.add_ggyy = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ggyy'/><input type='text' placeholder='自定义' style='width:45px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var ggyy_div = document.getElementById("ggyy_div");
        angular.element(ggyy_div).append(dynamicYwcpxElement);
    }

    $scope.add_hljt = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='hljt'/><input type='text' placeholder='自定义' style='width:70px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var hljt_div = document.getElementById("hljt_div");
        angular.element(hljt_div).append(dynamicYwcpxElement);
    }
});
//设置婚姻状况为单选
function hy_yh_select() {
    if (document.getElementById("hy_wh").checked) {
        document.getElementById("hy_wh").checked = false;
    }
}
function hy_wh_select() {
    if (document.getElementById("hy_yh").checked) {
        document.getElementById("hy_yh").checked = false;
    }
}

//设置生育为单选
function sy_yy_select() {
    if (document.getElementById("sy_wy").checked) {
        document.getElementById("sy_wy").checked = false;
    }
}
function sy_wy_select() {
    if (document.getElementById("sy_yy").checked) {
        document.getElementById("sy_yy").checked = false;
    }
}

//设置学历状况为单选
function xl_xx_select() {
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_cz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_zz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_gz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_dz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
}

//设置护理经验为单选
function hl_y_select() {
    if (document.getElementById("hl_w").checked) {
        document.getElementById("hl_w").checked = false;
    }
}
function hl_w_select() {
    if (document.getElementById("hl_y").checked) {
        document.getElementById("hl_y").checked = false;
    }
}

var selectedVal = document.getElementById('my-select');

function setSelectedCertification() {
    if (selectedVal.value == "myhls") {
        document.getElementById("certification-without-rank").style.display = "block";
        document.getElementById("certification-with-rank").style.display = "none";
        document.getElementById("certification-name-without-rank").innerHTML = "母婴护理师";
    }
    else if (selectedVal.value == "yyy") {
        document.getElementById("certification-with-rank").style.display = "block";
        document.getElementById("certification-without-rank").style.display = "none";
        document.getElementById("certification-name-with-rank").innerHTML = "育婴员资格证";
    }
    else if (selectedVal.value == "jzfwy") {
        document.getElementById("certification-with-rank").style.display = "block";
        document.getElementById("certification-without-rank").style.display = "none";
        document.getElementById("certification-name-with-rank").innerHTML = "家政服务员资格证";
    }
    else if (selectedVal.value == "yypcy") {
        document.getElementById("certification-with-rank").style.display = "block";
        document.getElementById("certification-without-rank").style.display = "none";
        document.getElementById("certification-name-with-rank").innerHTML = "营养配餐员资格证";
    }
    else if (selectedVal.value == "byy") {
        document.getElementById("certification-with-rank").style.display = "block";
        document.getElementById("certification-without-rank").style.display = "none";
        document.getElementById("certification-name-with-rank").innerHTML = "保育员资格证";
    }
    else if (selectedVal.value == "hsbyz") {
        document.getElementById("certification-without-rank").style.display = "block";
        document.getElementById("certification-with-rank").style.display = "none";
        document.getElementById("certification-name-without-rank").innerHTML = "护士毕业证";
    }
    else if (selectedVal.value == "crs") {
        document.getElementById("certification-without-rank").style.display = "block";
        document.getElementById("certification-with-rank").style.display = "none";
        document.getElementById("certification-name-without-rank").innerHTML = "催乳师";
    }
    else if (selectedVal.value == "xjys") {
        document.getElementById("certification-without-rank").style.display = "block";
        document.getElementById("certification-with-rank").style.display = "none";
        document.getElementById("certification-name-without-rank").innerHTML = "星际月嫂";
    }
    else if (selectedVal.value == "yys") {
        document.getElementById("certification-without-rank").style.display = "block";
        document.getElementById("certification-with-rank").style.display = "none";
        document.getElementById("certification-name-without-rank").innerHTML = "营养师";
    }
    else if (selectedVal.value == "myjkzds") {
        document.getElementById("certification-without-rank").style.display = "block";
        document.getElementById("certification-with-rank").style.display = "none";
        document.getElementById("certification-name-without-rank").innerHTML = "母婴健康指导师";
    }
}

function addMaterialImg() {
    $('div.material-img').parent().append(addMaterialImgTemplate);
    bind_upload();
}

function checkDate(start, end) {
    var startTime = start.getTime();
    var endTime = end.getTime();
    var curTime = new Date().getTime();
    return startTime<=curTime&&curTime<=endTime;
}
